//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2016.12.06 às 02:45:14 PM BRT 
//


package br.inf.portalfiscal.nfe;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de TCListServ.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="TCListServ">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;whiteSpace value="preserve"/>
 *     &lt;enumeration value="01.01"/>
 *     &lt;enumeration value="01.02"/>
 *     &lt;enumeration value="01.03"/>
 *     &lt;enumeration value="01.04"/>
 *     &lt;enumeration value="01.05"/>
 *     &lt;enumeration value="01.06"/>
 *     &lt;enumeration value="01.07"/>
 *     &lt;enumeration value="01.08"/>
 *     &lt;enumeration value="02.01"/>
 *     &lt;enumeration value="03.02"/>
 *     &lt;enumeration value="03.03"/>
 *     &lt;enumeration value="03.04"/>
 *     &lt;enumeration value="03.05"/>
 *     &lt;enumeration value="04.01"/>
 *     &lt;enumeration value="04.02"/>
 *     &lt;enumeration value="04.03"/>
 *     &lt;enumeration value="04.04"/>
 *     &lt;enumeration value="04.05"/>
 *     &lt;enumeration value="04.06"/>
 *     &lt;enumeration value="04.07"/>
 *     &lt;enumeration value="04.08"/>
 *     &lt;enumeration value="04.09"/>
 *     &lt;enumeration value="04.10"/>
 *     &lt;enumeration value="04.11"/>
 *     &lt;enumeration value="04.12"/>
 *     &lt;enumeration value="04.13"/>
 *     &lt;enumeration value="04.14"/>
 *     &lt;enumeration value="04.15"/>
 *     &lt;enumeration value="04.16"/>
 *     &lt;enumeration value="04.17"/>
 *     &lt;enumeration value="04.18"/>
 *     &lt;enumeration value="04.19"/>
 *     &lt;enumeration value="04.20"/>
 *     &lt;enumeration value="04.21"/>
 *     &lt;enumeration value="04.22"/>
 *     &lt;enumeration value="04.23"/>
 *     &lt;enumeration value="05.01"/>
 *     &lt;enumeration value="05.02"/>
 *     &lt;enumeration value="05.03"/>
 *     &lt;enumeration value="05.04"/>
 *     &lt;enumeration value="05.05"/>
 *     &lt;enumeration value="05.06"/>
 *     &lt;enumeration value="05.07"/>
 *     &lt;enumeration value="05.08"/>
 *     &lt;enumeration value="05.09"/>
 *     &lt;enumeration value="06.01"/>
 *     &lt;enumeration value="06.02"/>
 *     &lt;enumeration value="06.03"/>
 *     &lt;enumeration value="06.04"/>
 *     &lt;enumeration value="06.05"/>
 *     &lt;enumeration value="07.01"/>
 *     &lt;enumeration value="07.02"/>
 *     &lt;enumeration value="07.03"/>
 *     &lt;enumeration value="07.04"/>
 *     &lt;enumeration value="07.05"/>
 *     &lt;enumeration value="07.06"/>
 *     &lt;enumeration value="07.07"/>
 *     &lt;enumeration value="07.08"/>
 *     &lt;enumeration value="07.09"/>
 *     &lt;enumeration value="07.10"/>
 *     &lt;enumeration value="07.11"/>
 *     &lt;enumeration value="07.12"/>
 *     &lt;enumeration value="07.13"/>
 *     &lt;enumeration value="07.16"/>
 *     &lt;enumeration value="07.17"/>
 *     &lt;enumeration value="07.18"/>
 *     &lt;enumeration value="07.19"/>
 *     &lt;enumeration value="07.20"/>
 *     &lt;enumeration value="07.21"/>
 *     &lt;enumeration value="07.22"/>
 *     &lt;enumeration value="08.01"/>
 *     &lt;enumeration value="08.02"/>
 *     &lt;enumeration value="09.01"/>
 *     &lt;enumeration value="09.02"/>
 *     &lt;enumeration value="09.03"/>
 *     &lt;enumeration value="10.01"/>
 *     &lt;enumeration value="10.02"/>
 *     &lt;enumeration value="10.03"/>
 *     &lt;enumeration value="10.04"/>
 *     &lt;enumeration value="10.05"/>
 *     &lt;enumeration value="10.06"/>
 *     &lt;enumeration value="10.07"/>
 *     &lt;enumeration value="10.08"/>
 *     &lt;enumeration value="10.09"/>
 *     &lt;enumeration value="10.10"/>
 *     &lt;enumeration value="11.01"/>
 *     &lt;enumeration value="11.02"/>
 *     &lt;enumeration value="11.03"/>
 *     &lt;enumeration value="11.04"/>
 *     &lt;enumeration value="12.01"/>
 *     &lt;enumeration value="12.02"/>
 *     &lt;enumeration value="12.03"/>
 *     &lt;enumeration value="12.04"/>
 *     &lt;enumeration value="12.05"/>
 *     &lt;enumeration value="12.06"/>
 *     &lt;enumeration value="12.07"/>
 *     &lt;enumeration value="12.08"/>
 *     &lt;enumeration value="12.09"/>
 *     &lt;enumeration value="12.10"/>
 *     &lt;enumeration value="12.11"/>
 *     &lt;enumeration value="12.12"/>
 *     &lt;enumeration value="12.13"/>
 *     &lt;enumeration value="12.14"/>
 *     &lt;enumeration value="12.15"/>
 *     &lt;enumeration value="12.16"/>
 *     &lt;enumeration value="12.17"/>
 *     &lt;enumeration value="13.02"/>
 *     &lt;enumeration value="13.03"/>
 *     &lt;enumeration value="13.04"/>
 *     &lt;enumeration value="13.05"/>
 *     &lt;enumeration value="14.01"/>
 *     &lt;enumeration value="14.02"/>
 *     &lt;enumeration value="14.03"/>
 *     &lt;enumeration value="14.04"/>
 *     &lt;enumeration value="14.05"/>
 *     &lt;enumeration value="14.06"/>
 *     &lt;enumeration value="14.07"/>
 *     &lt;enumeration value="14.08"/>
 *     &lt;enumeration value="14.09"/>
 *     &lt;enumeration value="14.10"/>
 *     &lt;enumeration value="14.11"/>
 *     &lt;enumeration value="14.12"/>
 *     &lt;enumeration value="14.13"/>
 *     &lt;enumeration value="15.01"/>
 *     &lt;enumeration value="15.02"/>
 *     &lt;enumeration value="15.03"/>
 *     &lt;enumeration value="15.04"/>
 *     &lt;enumeration value="15.05"/>
 *     &lt;enumeration value="15.06"/>
 *     &lt;enumeration value="15.07"/>
 *     &lt;enumeration value="15.08"/>
 *     &lt;enumeration value="15.09"/>
 *     &lt;enumeration value="15.10"/>
 *     &lt;enumeration value="15.11"/>
 *     &lt;enumeration value="15.12"/>
 *     &lt;enumeration value="15.13"/>
 *     &lt;enumeration value="15.14"/>
 *     &lt;enumeration value="15.15"/>
 *     &lt;enumeration value="15.16"/>
 *     &lt;enumeration value="15.17"/>
 *     &lt;enumeration value="15.18"/>
 *     &lt;enumeration value="16.01"/>
 *     &lt;enumeration value="17.01"/>
 *     &lt;enumeration value="17.02"/>
 *     &lt;enumeration value="17.03"/>
 *     &lt;enumeration value="17.04"/>
 *     &lt;enumeration value="17.05"/>
 *     &lt;enumeration value="17.06"/>
 *     &lt;enumeration value="17.08"/>
 *     &lt;enumeration value="17.09"/>
 *     &lt;enumeration value="17.10"/>
 *     &lt;enumeration value="17.11"/>
 *     &lt;enumeration value="17.12"/>
 *     &lt;enumeration value="17.13"/>
 *     &lt;enumeration value="17.14"/>
 *     &lt;enumeration value="17.15"/>
 *     &lt;enumeration value="17.16"/>
 *     &lt;enumeration value="17.17"/>
 *     &lt;enumeration value="17.18"/>
 *     &lt;enumeration value="17.19"/>
 *     &lt;enumeration value="17.20"/>
 *     &lt;enumeration value="17.21"/>
 *     &lt;enumeration value="17.22"/>
 *     &lt;enumeration value="17.23"/>
 *     &lt;enumeration value="17.24"/>
 *     &lt;enumeration value="18.01"/>
 *     &lt;enumeration value="19.01"/>
 *     &lt;enumeration value="20.01"/>
 *     &lt;enumeration value="20.02"/>
 *     &lt;enumeration value="20.03"/>
 *     &lt;enumeration value="21.01"/>
 *     &lt;enumeration value="22.01"/>
 *     &lt;enumeration value="23.01"/>
 *     &lt;enumeration value="24.01"/>
 *     &lt;enumeration value="25.01"/>
 *     &lt;enumeration value="25.02"/>
 *     &lt;enumeration value="25.03"/>
 *     &lt;enumeration value="25.04"/>
 *     &lt;enumeration value="26.01"/>
 *     &lt;enumeration value="27.01"/>
 *     &lt;enumeration value="28.01"/>
 *     &lt;enumeration value="29.01"/>
 *     &lt;enumeration value="30.01"/>
 *     &lt;enumeration value="31.01"/>
 *     &lt;enumeration value="32.01"/>
 *     &lt;enumeration value="33.01"/>
 *     &lt;enumeration value="34.01"/>
 *     &lt;enumeration value="35.01"/>
 *     &lt;enumeration value="36.01"/>
 *     &lt;enumeration value="37.01"/>
 *     &lt;enumeration value="38.01"/>
 *     &lt;enumeration value="39.01"/>
 *     &lt;enumeration value="40.01"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TCListServ")
@XmlEnum
public enum TCListServ {

    @XmlEnumValue("01.01")
    VALUE_1("01.01"),
    @XmlEnumValue("01.02")
    VALUE_2("01.02"),
    @XmlEnumValue("01.03")
    VALUE_3("01.03"),
    @XmlEnumValue("01.04")
    VALUE_4("01.04"),
    @XmlEnumValue("01.05")
    VALUE_5("01.05"),
    @XmlEnumValue("01.06")
    VALUE_6("01.06"),
    @XmlEnumValue("01.07")
    VALUE_7("01.07"),
    @XmlEnumValue("01.08")
    VALUE_8("01.08"),
    @XmlEnumValue("02.01")
    VALUE_9("02.01"),
    @XmlEnumValue("03.02")
    VALUE_10("03.02"),
    @XmlEnumValue("03.03")
    VALUE_11("03.03"),
    @XmlEnumValue("03.04")
    VALUE_12("03.04"),
    @XmlEnumValue("03.05")
    VALUE_13("03.05"),
    @XmlEnumValue("04.01")
    VALUE_14("04.01"),
    @XmlEnumValue("04.02")
    VALUE_15("04.02"),
    @XmlEnumValue("04.03")
    VALUE_16("04.03"),
    @XmlEnumValue("04.04")
    VALUE_17("04.04"),
    @XmlEnumValue("04.05")
    VALUE_18("04.05"),
    @XmlEnumValue("04.06")
    VALUE_19("04.06"),
    @XmlEnumValue("04.07")
    VALUE_20("04.07"),
    @XmlEnumValue("04.08")
    VALUE_21("04.08"),
    @XmlEnumValue("04.09")
    VALUE_22("04.09"),
    @XmlEnumValue("04.10")
    VALUE_23("04.10"),
    @XmlEnumValue("04.11")
    VALUE_24("04.11"),
    @XmlEnumValue("04.12")
    VALUE_25("04.12"),
    @XmlEnumValue("04.13")
    VALUE_26("04.13"),
    @XmlEnumValue("04.14")
    VALUE_27("04.14"),
    @XmlEnumValue("04.15")
    VALUE_28("04.15"),
    @XmlEnumValue("04.16")
    VALUE_29("04.16"),
    @XmlEnumValue("04.17")
    VALUE_30("04.17"),
    @XmlEnumValue("04.18")
    VALUE_31("04.18"),
    @XmlEnumValue("04.19")
    VALUE_32("04.19"),
    @XmlEnumValue("04.20")
    VALUE_33("04.20"),
    @XmlEnumValue("04.21")
    VALUE_34("04.21"),
    @XmlEnumValue("04.22")
    VALUE_35("04.22"),
    @XmlEnumValue("04.23")
    VALUE_36("04.23"),
    @XmlEnumValue("05.01")
    VALUE_37("05.01"),
    @XmlEnumValue("05.02")
    VALUE_38("05.02"),
    @XmlEnumValue("05.03")
    VALUE_39("05.03"),
    @XmlEnumValue("05.04")
    VALUE_40("05.04"),
    @XmlEnumValue("05.05")
    VALUE_41("05.05"),
    @XmlEnumValue("05.06")
    VALUE_42("05.06"),
    @XmlEnumValue("05.07")
    VALUE_43("05.07"),
    @XmlEnumValue("05.08")
    VALUE_44("05.08"),
    @XmlEnumValue("05.09")
    VALUE_45("05.09"),
    @XmlEnumValue("06.01")
    VALUE_46("06.01"),
    @XmlEnumValue("06.02")
    VALUE_47("06.02"),
    @XmlEnumValue("06.03")
    VALUE_48("06.03"),
    @XmlEnumValue("06.04")
    VALUE_49("06.04"),
    @XmlEnumValue("06.05")
    VALUE_50("06.05"),
    @XmlEnumValue("07.01")
    VALUE_51("07.01"),
    @XmlEnumValue("07.02")
    VALUE_52("07.02"),
    @XmlEnumValue("07.03")
    VALUE_53("07.03"),
    @XmlEnumValue("07.04")
    VALUE_54("07.04"),
    @XmlEnumValue("07.05")
    VALUE_55("07.05"),
    @XmlEnumValue("07.06")
    VALUE_56("07.06"),
    @XmlEnumValue("07.07")
    VALUE_57("07.07"),
    @XmlEnumValue("07.08")
    VALUE_58("07.08"),
    @XmlEnumValue("07.09")
    VALUE_59("07.09"),
    @XmlEnumValue("07.10")
    VALUE_60("07.10"),
    @XmlEnumValue("07.11")
    VALUE_61("07.11"),
    @XmlEnumValue("07.12")
    VALUE_62("07.12"),
    @XmlEnumValue("07.13")
    VALUE_63("07.13"),
    @XmlEnumValue("07.16")
    VALUE_64("07.16"),
    @XmlEnumValue("07.17")
    VALUE_65("07.17"),
    @XmlEnumValue("07.18")
    VALUE_66("07.18"),
    @XmlEnumValue("07.19")
    VALUE_67("07.19"),
    @XmlEnumValue("07.20")
    VALUE_68("07.20"),
    @XmlEnumValue("07.21")
    VALUE_69("07.21"),
    @XmlEnumValue("07.22")
    VALUE_70("07.22"),
    @XmlEnumValue("08.01")
    VALUE_71("08.01"),
    @XmlEnumValue("08.02")
    VALUE_72("08.02"),
    @XmlEnumValue("09.01")
    VALUE_73("09.01"),
    @XmlEnumValue("09.02")
    VALUE_74("09.02"),
    @XmlEnumValue("09.03")
    VALUE_75("09.03"),
    @XmlEnumValue("10.01")
    VALUE_76("10.01"),
    @XmlEnumValue("10.02")
    VALUE_77("10.02"),
    @XmlEnumValue("10.03")
    VALUE_78("10.03"),
    @XmlEnumValue("10.04")
    VALUE_79("10.04"),
    @XmlEnumValue("10.05")
    VALUE_80("10.05"),
    @XmlEnumValue("10.06")
    VALUE_81("10.06"),
    @XmlEnumValue("10.07")
    VALUE_82("10.07"),
    @XmlEnumValue("10.08")
    VALUE_83("10.08"),
    @XmlEnumValue("10.09")
    VALUE_84("10.09"),
    @XmlEnumValue("10.10")
    VALUE_85("10.10"),
    @XmlEnumValue("11.01")
    VALUE_86("11.01"),
    @XmlEnumValue("11.02")
    VALUE_87("11.02"),
    @XmlEnumValue("11.03")
    VALUE_88("11.03"),
    @XmlEnumValue("11.04")
    VALUE_89("11.04"),
    @XmlEnumValue("12.01")
    VALUE_90("12.01"),
    @XmlEnumValue("12.02")
    VALUE_91("12.02"),
    @XmlEnumValue("12.03")
    VALUE_92("12.03"),
    @XmlEnumValue("12.04")
    VALUE_93("12.04"),
    @XmlEnumValue("12.05")
    VALUE_94("12.05"),
    @XmlEnumValue("12.06")
    VALUE_95("12.06"),
    @XmlEnumValue("12.07")
    VALUE_96("12.07"),
    @XmlEnumValue("12.08")
    VALUE_97("12.08"),
    @XmlEnumValue("12.09")
    VALUE_98("12.09"),
    @XmlEnumValue("12.10")
    VALUE_99("12.10"),
    @XmlEnumValue("12.11")
    VALUE_100("12.11"),
    @XmlEnumValue("12.12")
    VALUE_101("12.12"),
    @XmlEnumValue("12.13")
    VALUE_102("12.13"),
    @XmlEnumValue("12.14")
    VALUE_103("12.14"),
    @XmlEnumValue("12.15")
    VALUE_104("12.15"),
    @XmlEnumValue("12.16")
    VALUE_105("12.16"),
    @XmlEnumValue("12.17")
    VALUE_106("12.17"),
    @XmlEnumValue("13.02")
    VALUE_107("13.02"),
    @XmlEnumValue("13.03")
    VALUE_108("13.03"),
    @XmlEnumValue("13.04")
    VALUE_109("13.04"),
    @XmlEnumValue("13.05")
    VALUE_110("13.05"),
    @XmlEnumValue("14.01")
    VALUE_111("14.01"),
    @XmlEnumValue("14.02")
    VALUE_112("14.02"),
    @XmlEnumValue("14.03")
    VALUE_113("14.03"),
    @XmlEnumValue("14.04")
    VALUE_114("14.04"),
    @XmlEnumValue("14.05")
    VALUE_115("14.05"),
    @XmlEnumValue("14.06")
    VALUE_116("14.06"),
    @XmlEnumValue("14.07")
    VALUE_117("14.07"),
    @XmlEnumValue("14.08")
    VALUE_118("14.08"),
    @XmlEnumValue("14.09")
    VALUE_119("14.09"),
    @XmlEnumValue("14.10")
    VALUE_120("14.10"),
    @XmlEnumValue("14.11")
    VALUE_121("14.11"),
    @XmlEnumValue("14.12")
    VALUE_122("14.12"),
    @XmlEnumValue("14.13")
    VALUE_123("14.13"),
    @XmlEnumValue("15.01")
    VALUE_124("15.01"),
    @XmlEnumValue("15.02")
    VALUE_125("15.02"),
    @XmlEnumValue("15.03")
    VALUE_126("15.03"),
    @XmlEnumValue("15.04")
    VALUE_127("15.04"),
    @XmlEnumValue("15.05")
    VALUE_128("15.05"),
    @XmlEnumValue("15.06")
    VALUE_129("15.06"),
    @XmlEnumValue("15.07")
    VALUE_130("15.07"),
    @XmlEnumValue("15.08")
    VALUE_131("15.08"),
    @XmlEnumValue("15.09")
    VALUE_132("15.09"),
    @XmlEnumValue("15.10")
    VALUE_133("15.10"),
    @XmlEnumValue("15.11")
    VALUE_134("15.11"),
    @XmlEnumValue("15.12")
    VALUE_135("15.12"),
    @XmlEnumValue("15.13")
    VALUE_136("15.13"),
    @XmlEnumValue("15.14")
    VALUE_137("15.14"),
    @XmlEnumValue("15.15")
    VALUE_138("15.15"),
    @XmlEnumValue("15.16")
    VALUE_139("15.16"),
    @XmlEnumValue("15.17")
    VALUE_140("15.17"),
    @XmlEnumValue("15.18")
    VALUE_141("15.18"),
    @XmlEnumValue("16.01")
    VALUE_142("16.01"),
    @XmlEnumValue("17.01")
    VALUE_143("17.01"),
    @XmlEnumValue("17.02")
    VALUE_144("17.02"),
    @XmlEnumValue("17.03")
    VALUE_145("17.03"),
    @XmlEnumValue("17.04")
    VALUE_146("17.04"),
    @XmlEnumValue("17.05")
    VALUE_147("17.05"),
    @XmlEnumValue("17.06")
    VALUE_148("17.06"),
    @XmlEnumValue("17.08")
    VALUE_149("17.08"),
    @XmlEnumValue("17.09")
    VALUE_150("17.09"),
    @XmlEnumValue("17.10")
    VALUE_151("17.10"),
    @XmlEnumValue("17.11")
    VALUE_152("17.11"),
    @XmlEnumValue("17.12")
    VALUE_153("17.12"),
    @XmlEnumValue("17.13")
    VALUE_154("17.13"),
    @XmlEnumValue("17.14")
    VALUE_155("17.14"),
    @XmlEnumValue("17.15")
    VALUE_156("17.15"),
    @XmlEnumValue("17.16")
    VALUE_157("17.16"),
    @XmlEnumValue("17.17")
    VALUE_158("17.17"),
    @XmlEnumValue("17.18")
    VALUE_159("17.18"),
    @XmlEnumValue("17.19")
    VALUE_160("17.19"),
    @XmlEnumValue("17.20")
    VALUE_161("17.20"),
    @XmlEnumValue("17.21")
    VALUE_162("17.21"),
    @XmlEnumValue("17.22")
    VALUE_163("17.22"),
    @XmlEnumValue("17.23")
    VALUE_164("17.23"),
    @XmlEnumValue("17.24")
    VALUE_165("17.24"),
    @XmlEnumValue("18.01")
    VALUE_166("18.01"),
    @XmlEnumValue("19.01")
    VALUE_167("19.01"),
    @XmlEnumValue("20.01")
    VALUE_168("20.01"),
    @XmlEnumValue("20.02")
    VALUE_169("20.02"),
    @XmlEnumValue("20.03")
    VALUE_170("20.03"),
    @XmlEnumValue("21.01")
    VALUE_171("21.01"),
    @XmlEnumValue("22.01")
    VALUE_172("22.01"),
    @XmlEnumValue("23.01")
    VALUE_173("23.01"),
    @XmlEnumValue("24.01")
    VALUE_174("24.01"),
    @XmlEnumValue("25.01")
    VALUE_175("25.01"),
    @XmlEnumValue("25.02")
    VALUE_176("25.02"),
    @XmlEnumValue("25.03")
    VALUE_177("25.03"),
    @XmlEnumValue("25.04")
    VALUE_178("25.04"),
    @XmlEnumValue("26.01")
    VALUE_179("26.01"),
    @XmlEnumValue("27.01")
    VALUE_180("27.01"),
    @XmlEnumValue("28.01")
    VALUE_181("28.01"),
    @XmlEnumValue("29.01")
    VALUE_182("29.01"),
    @XmlEnumValue("30.01")
    VALUE_183("30.01"),
    @XmlEnumValue("31.01")
    VALUE_184("31.01"),
    @XmlEnumValue("32.01")
    VALUE_185("32.01"),
    @XmlEnumValue("33.01")
    VALUE_186("33.01"),
    @XmlEnumValue("34.01")
    VALUE_187("34.01"),
    @XmlEnumValue("35.01")
    VALUE_188("35.01"),
    @XmlEnumValue("36.01")
    VALUE_189("36.01"),
    @XmlEnumValue("37.01")
    VALUE_190("37.01"),
    @XmlEnumValue("38.01")
    VALUE_191("38.01"),
    @XmlEnumValue("39.01")
    VALUE_192("39.01"),
    @XmlEnumValue("40.01")
    VALUE_193("40.01");
    private final String value;

    TCListServ(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TCListServ fromValue(String v) {
        for (TCListServ c: TCListServ.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
