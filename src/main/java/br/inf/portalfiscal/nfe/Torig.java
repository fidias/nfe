//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2016.12.06 às 02:45:14 PM BRT 
//


package br.inf.portalfiscal.nfe;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Torig.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="Torig">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;whiteSpace value="preserve"/>
 *     &lt;enumeration value="0"/>
 *     &lt;enumeration value="1"/>
 *     &lt;enumeration value="2"/>
 *     &lt;enumeration value="3"/>
 *     &lt;enumeration value="4"/>
 *     &lt;enumeration value="5"/>
 *     &lt;enumeration value="6"/>
 *     &lt;enumeration value="7"/>
 *     &lt;enumeration value="8"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Torig")
@XmlEnum
public enum Torig {

    @XmlEnumValue("0")
    VALUE_1("0"),
    @XmlEnumValue("1")
    VALUE_2("1"),
    @XmlEnumValue("2")
    VALUE_3("2"),
    @XmlEnumValue("3")
    VALUE_4("3"),
    @XmlEnumValue("4")
    VALUE_5("4"),
    @XmlEnumValue("5")
    VALUE_6("5"),
    @XmlEnumValue("6")
    VALUE_7("6"),
    @XmlEnumValue("7")
    VALUE_8("7"),
    @XmlEnumValue("8")
    VALUE_9("8");
    private final String value;

    Torig(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Torig fromValue(String v) {
        for (Torig c: Torig.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
