# NFe

Projeto para geração e compilação das Classes Java
referentes ao projeto NFe.

Este projeto utiliza Maven2.

## XSD - Passo a passo

Baixar arquivos de <http://www.nfe.fazenda.gov.br/portal/listaConteudo.aspx?tipoConteudo=/fwLvLUSmU8=>.

Substituir arquivos baixados no diretório `schema`.

Num terminal executar:

```bash
xjc -nv -d src/main/java/ \
    schemas/4.00/nfe_v4.00.xsd \
    -p br.inf.portalfiscal.nfe400
```

```bash
xjc -nv -d src/main/java/ \
    schemas/4.00/distDFeInt_v1.01.xsd \
    schemas/4.00/resNFe_v1.01.xsd \
    schemas/4.00/resEvento_v1.01.xsd \
    schemas/4.00/retDistDFeInt_v1.01.xsd \
    -p br.inf.portalfiscal.nfe400.distdfe
```

```bash
xjc -nv -d src/main/java/ \
    schemas/4.00/inutNFe_v4.00.xsd \
    -p br.inf.portalfiscal.nfe400.inut
```

1. `xjc -d src/main/java/ schemas/nfe_v3.10.xsd -p br.inf.portalfiscal.nfe`
1. `xjc -d src/main/java/ schemas/envEventoCancNFe_v1.00.xsd  -p br.inf.portalfiscal.evento.canc -b schemas-conf/000DefaultBinding.xsd`
1. `xjc -d src/main/java/ schemas/carta-correcao/envCCe_v1.00.xsd -p br.inf.portalfiscal.nfe.evento.correcao`
1. `xjc -d src/main/java/ schemas/evento-generico/retEnvEvento_v1.00.xsd -p br.inf.portalfiscal.evento.manifestacao -b schemas-conf/000DefaultBinding.xsd `

**OBS.**: Pode ser que seja necessário modificar a versão!

## WSDL

### Links

* <http://nfe.sefaz.ce.gov.br/pages/informacoes/web_services.jsf> - Lista web services
* <http://www.javac.com.br/jc/posts/list/30/1867.page> - Exemplo uso Axis2

## Passo a passo

Versão 3.10

`~/dev-lib/java/axis2-1.5.6/bin/wsdl2java.sh -S src/main/java/ -uri wsdl/NfeInutilizacao2.wsdl`

Versão 4.00

```bash
~/dev-lib/java/axis2-1.5.6/bin/wsdl2java.sh \
    -S src/main/java/ \
    -uri wsdl/4.00/NFeAutorizacao4.wsdl
# inutilização
~/dev-lib/java/axis2-1.5.6/bin/wsdl2java.sh \
      -S src/main/java/ \
      -uri wsdl/4.00/NFeInutilizacao4.wsdl
```

## Dica

Para melhor performance de validação de arquivos XML com XSD, substituir valores
altos de atributos `maxOccurs` por `unbounded`.

Em testes feitos com a NFe, o tempo de validação foi de 10s (javax.xml)
para 496 ms.

De qualquer forma o documento será validado pelo web service, assim fica melhor
para o cliente - onde a execução é mais rápida, e para a fidias - menos gastos
com CPU.
